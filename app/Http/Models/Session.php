<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Session extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'session';

    protected $fillable = ['*'];
}