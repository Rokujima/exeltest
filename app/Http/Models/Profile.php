<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model as Model;
use DB;

class Profile extends Model
{
    
	public $table = "profile";
    
	public $timestamps = false;
	
	public $fillable = [
		"name",
        "email",
        "jeniskelamin",
        "tgl_lahir",
        "alamat",
        "hobi"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
		"name" => "string",
		"email" => "string",
        "jeniskelamin" => "string",
        "tgl_lahir" => "date",
        "alamat" => "text",
        "hobi" => "text"
        
    ];

	public static $rules = [
	    "name" => "required",
        "email" => "required",
        "jeniskelamin" => "required",
        "tgl_lahir" => "required",
        "alamat" => "required"
	];
	
	public static $rules_update = [
        "id" => "required",
        "name" => "required",
        "email" => "required"
	];


}
