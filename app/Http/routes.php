<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });




Route::group(['prefix' => ''], function(){
    
    Route::get('/', ['as' => 'index', 'uses' => 'CRUDController@index']);
    Route::get('_get', ['as' => 'get', 'uses' => 'CRUDController@get_']);
    
    Route::post('store', ['as' => 'insert', 'uses' => 'CRUDController@Store']);
    Route::put('update', ['as' => 'update', 'uses' => 'CRUDController@Put']);
    Route::delete('delete',['as' => 'delete', 'uses' => 'CRUDController@Delete']);

});