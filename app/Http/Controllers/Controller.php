<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;


    public function __construct()
    {
    
    }


    public function statusCode($code){

        $statusCode = array(
            '1A'	=> '[Register IP]',
            '1B'	=> '[Open Page]',
            '1C'	=> '[ReOpen Page]',

            '2A'	=> '[Create Data]',
            '2B'	=> '[Update Data]',
            '2C'	=> '[Read Data]',
            '2D'	=> '[Delete Data]'
        );
    
        return $statusCode[$code];

    }


}
