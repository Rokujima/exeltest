<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use Input;
use Cache;
use Redirect;
use Validator;
use Carbon\Carbon;
use Session;
use Storage;
use Illuminate\Database\Eloquent\Collection;
use Datatables;

use App\Http\Models\Session as sessions;
use App\Http\Models\Profile;


class CRUDController extends Controller
{  
    public function index(Request $request)
    {
        return View::make('index');
    }

    public function get_(Request $request)
    {
        
        $id = $request->input('id');

        if(!empty($id)){
            $get = Profile::where('id', $id)->first();
        }else{
            $get = Profile::all();
        }
        
        return $get;
    }

    public function Store(Request $request)
    {
        
        $dt         = Carbon::now('Asia/Jakarta');
        
        $validator     = Validator::make($request->all(), [
            'name'          => 'required',
            'email'         => 'required',
            'jenis'         => 'required',
            'tgl_lahir'     => 'required',
            'alamat'        => 'required'
        ]);
       
        if ($validator->fails()) 
        {
            $error = $validator->errors();
            return json_encode(array('status' => 'fail', 'message' => $error->all()[0]));
        }
        else
        {
            $mailValidat = Profile::where('email',input::get('email'))->first();
            if(!empty($mailValidat)){
                return json_encode(array('status' => 'fail', 'message' => "Request failed Mail Duplicate"));
            }
            
            $insert = new Profile();
            $insert->name             = input::get('name');
            $insert->email            = input::get('email');
            $insert->jeniskelamin     = Input::get('jenis');
            $insert->tgl_lahir        = date("Y-m-d", strtotime(str_replace("/","-",Input::get('tgl_lahir'))));
            $insert->alamat           = Input::get('alamat');
            $insert->hobi             = implode(",",Input::get('hobi'));
            $insert->created_at       = $dt;
            $insert->created_by       = $_SERVER['REMOTE_ADDR'];
            $insert->save();

            if(!empty($insert)){
                return json_encode(array('status' => 'success', 'message' => "Request create successful"));
            }else{
                return json_encode(array('status' => 'fail', 'message' => "Request failed"));
            }
        
        }
    }

    public function Put(Request $request)
    {
        $dt         = Carbon::now('Asia/Jakarta');
        
        $validator     = Validator::make($request->all(), [
            'id'            => 'required',
            'name'          => 'required',
            'jeniskelamin'  => 'required',
            'tgl_lahir'     => 'required',
            'alamat'        => 'required'
        ]);

        if ($validator->fails()) 
        {
            $error = $validator->errors();
            return json_encode(array('status' => 'fail', 'message' => $error->all()[0]));
        }
        else
        {

            $id     = Input::get('id');

            $put = Profile::find($id);
            $put->name             = input::get('name');
            $put->email            = input::get('email');
            $put->jeniskelamin     = Input::get('jeniskelamin');
            $put->tgl_lahir        = date("Y-m-d", strtotime(str_replace("/","-",Input::get('tgl_lahir'))));
            $put->alamat           = Input::get('alamat');
            $put->hobi             = Input::get('hobi');
            $put->updated_at       = $dt;
            $put->updated_by       = $_SERVER['REMOTE_ADDR'];
            $put->save();

            // if(!empty($put)){
            //     return json_encode($put);
            // }else{
            //     return json_encode(array('status' => 'fail', 'message' => "Request failed"));
            // }
        }
    }

    public function Delete(Request $request)
    {
        
        $validator     = Validator::make($request->all(), [
            'id'            => 'required'
        ]);

        if ($validator->fails()) 
        {
            $error = $validator->errors();
            return json_encode(array('status' => 'fail', 'message' => $error->all()[0]));
        }
        else
        {

            $id            = Input::get('id');

            $result        = Profile::destroy($id);

            if(!empty($result)){
                return json_encode(array('status' => 'success', 'message' => "Request Delete"));
            }else{
                return json_encode(array('status' => 'fail', 'message' => "Request failed"));
            }
        }
    }

    
}
