<!DOCTYPE html>
<html>
  
<head>
    <title>
      Exel Test Crud
    </title>
    <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/font-awesome.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/se7en-font.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/isotope.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/jquery.fancybox.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/fullcalendar.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/wizard.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/select2.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/morris.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/datatables.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/datepicker.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/timepicker.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/colorpicker.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/bootstrap-switch.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/daterange-picker.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/typeahead.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/summernote.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/pygments.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/style.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/color/green.css') }}" media="all" rel="alternate stylesheet" title="green-theme" type="text/css" />
    <link href="{{ asset('asset/css/color/orange.css') }}" media="all" rel="alternate stylesheet" title="orange-theme" type="text/css" />
    <link href="{{ asset('asset/css/color/magenta.css') }}" media="all" rel="alternate stylesheet" title="magenta-theme" type="text/css" />
    <link href="{{ asset('asset/css/color/gray.css') }}" media="all" rel="alternate stylesheet" title="gray-theme" type="text/css" />
    <script src="{{ asset('asset/js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/jquery-ui.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/raphael.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/selectivizr-min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/jquery.mousewheel.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/jquery.vmap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/jquery.vmap.sampledata.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/jquery.vmap.world.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/fullcalendar.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/gcal.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/datatable-editable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/jquery.easy-pie-chart.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/excanvas.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/jquery.isotope.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/isotope_extras.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/modernizr.custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/styleswitcher.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/wysiwyg.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/summernote.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/jquery.inputmask.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/jquery.validate.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/bootstrap-fileupload.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/bootstrap-timepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/bootstrap-colorpicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/typeahead.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/daterange-picker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/date.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/skycons.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/fitvids.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/jquery.sparkline.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/main.js') }}" type="text/javascript"></script>
    <script src="{{ asset('asset/js/respond.js') }}" type="text/javascript"></script>
    {{--  Form  --}}
    <script src="{{ asset('asset/js/jquery.form/jquery.form.js') }}"></script>
    {{--  Jsgrid  --}}
    <script src="{{ asset('asset/js/jsgrid/js/jsgrid.min.js') }}"></script>
    
    <link rel="stylesheet" href="{{ asset('asset/js/jsgrid/css/jsgrid.min.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/js/jsgrid/css/jsgrid-theme.min.css') }}">

    <link rel="stylesheet" href="{{ asset('asset/js/multiselect/js/jquery.multi-select.js') }}">
    <link rel="stylesheet" href="{{ asset('asset/js/multiselect/css/multi-select.css') }}">

    
    <script src="{{ asset('asset/js/moment/moment.min.js') }}"></script>

    <!-- Sweetalert CSS -->
    <link href="{{ asset('asset/js/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
    
    <!-- Sweetalert -->
    <script src="{{ asset('asset/js/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    
    
    


    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
  </head>
  <body>
    <div class="modal-shiftfix">
      <!-- Navigation -->
      <div class="navbar navbar-fixed-top scroll-hide">
        <div class="container-fluid top-bar">
         
          <button class="navbar-toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a href="{{ route('index')}}"><h1>Exel</h1></a>
          
        </div>


        
      </div>
      <!-- End Navigation -->
      <div class="container-fluid main-content"><div class="page-title">
  <h1>
    Form
  </h1>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="widget-container fluid-height clearfix">
      <div class="heading">
        <i class="icon-reorder"></i>Biodata
      </div>
      <div class="widget-content padded">

      {!! Form::open(['route' => 'insert', 'class' => "form-horizontal", 'method' =>'POST', 'files' => 'true', 'id' => 'myForm']) !!}

        <form action="#" class="form-horizontal">

        <div class="form-group">
            {!! Form::label('name', 'Name * ', ['class' => 'control-label col-md-2']) !!}
            <div class="input-group col-md-7">
                 {!! Form::text('name', null ,['class' => 'form-control ','id' => 'name', 'required' => "required", 'placeholder' => 'Enter your name....']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('email', 'Email * ', ['class' => 'control-label col-md-2']) !!}
            <div class="input-group col-md-7">
                 {!! Form::email('email', null ,['class' => 'form-control ','id' => 'email', 'required' => "required", 'placeholder' => 'Enter a email....']) !!}
            </div>
        </div>

        <div class="form-group">
                {!! Form::label('jenis', 'Jenis Kelamin *', ['class' => 'control-label col-md-2']) !!}
                                    
                <div class="col-md-7">
                    
                    <label class="radio" for="option1">
                    {!! Form::radio('jenis', 'Laki - Laki', 'checked', ['class' => 'option1', 'id' => 'option1']) !!}
                    <span>Laki - Laki</span>
                    </label>

                    <label class="radio" for="option2">
                    {!! Form::radio('jenis', 'Perempuan', null, ['class' => 'option2',  'id' => 'option2']) !!}
                    <span>Perempuan</span>
                    </label>

                </div>
        </div>

        <div class="form-group">
            {!! Form::label('tgl_lahir', 'Tanggal Lahir *', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-3">
              <div class="input-group date datepicker">
                <input class="form-control" type="text" name="tgl_lahir" required><span class="input-group-addon"><i class="icon-calendar"></i></span></input>
              </div>
            </div>
          </div>

          <div class="form-group">
            {!! Form::label('alamat', 'Alamat *', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-7">
            
            {!! Form::textarea('alamat', null, ['required' => "required", 'row' => 3, 'class' => 'form-control']) !!}
            
            </div>
          </div>

          <div class="form-group">
            {!! Form::label('hobi', 'Hobi *', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-7">
              <select class="select2able" multiple="" name="hobi[]">
              <option value="Olahraga">Olah raga</option>
              <option value="Membaca">Membaca</option>
              <option value="Main game">Main game</option>
              <option value="Music">Music</option>

              ;;;
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-2"></label>
            <div class="col-md-7">
              <button class="btn btn-primary load" type="submit">Submit</button><button class="btn btn-default-outline" type="reset">Cancel</button>
            </div>
          </div>

        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>



<div class="row">
  <div class="col-lg-12">
    <div class="widget-container fluid-height clearfix">
      <div class="heading">
        <i class="icon-collapse"></i>List
      </div>
      <div class="widget-content padded">
        
        {{--  table  --}}

        <div id="jsGrid-table"></div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="dlgDetails">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">&times;</button>
        <h4 class="modal-title">
          Details
        </h4>
      </div>
      <div class="modal-body">
        <p><label>Name: <span id="names"></span></label></p>
        <p><label>Email: <span id="emails"></span></label></p>
        <p><label>Jenis Kelamin: <span id="jenis"></span></label></p>
        <p><label>Tanggal Lahir: <span id="tanggal"></span></label></p>
        <p><label>Hobi: <span id="hobis"></span></label></p>
        <p><label>Alamat: <span id="alamats"></span></label></p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button">Save Changes</button><button class="btn btn-default-outline" data-dismiss="modal" type="button">Close</button>
      </div>
    </div>
  </div>
</div>

      </div>
    </div>
  </body>
  <script>
  $(document).ready(function() {

  $('#dlgDetails').hide();

  $('.datepicker').datepicker(
    {
      dateFormat: 'dd-mm-yy',
      currentText: "Now"
    }
    );

  let jenis = [
      { Name: "", Id: 0 },
      { Name: "Laki - Laki", Id: 1 },
      { Name: "Perempuan", Id: 2 }
  ];

  let Hobi = [
      { Name: "", Id: 0 },
      { Name: "Olahraga", Id: 1 },
      { Name: "Membaca", Id: 2 },
      { Name: "Main game", Id: 3 },
      { Name: "Music", Id: 4 }
  ];

  var DateField = function(config) {
    jsGrid.Field.call(this, config);
  };

DateField.prototype = new jsGrid.Field({
    sorter: function(date1, date2) {
        return new Date(date1) - new Date(date2);
    },    
    
    itemTemplate: function(value) {
        return new Date(value).toDateString();
    },
    
    filterTemplate: function() {
        var now = new Date();
        this._fromPicker = $("<input>").datepicker({ defaultDate: now.setFullYear(now.getFullYear() - 1) });
        this._toPicker = $("<input>").datepicker({ defaultDate: now.setFullYear(now.getFullYear() + 1) });
        return $("<div>").append(this._fromPicker).append(this._toPicker);
    },
    
    insertTemplate: function(value) {
        return this._insertPicker = $("<input>").datepicker({ defaultDate: new Date() });
    },
    
    editTemplate: function(value) {
        return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
    },
    
    insertValue: function() {
        return this._insertPicker.datepicker("getDate").toISOString();
    },
    
    editValue: function() {
        return this._editPicker.datepicker("getDate").toISOString();
    },
    
    filterValue: function() {
        return {
            from: this._fromPicker.datepicker("getDate"),
            to: this._toPicker.datepicker("getDate")
        };
    }
});

jsGrid.fields.date = DateField;

var MultiselectField = function(config) {
    jsGrid.Field.call(this, config);
};

MultiselectField.prototype = new jsGrid.Field({
    
    items: [],
    textField: "",
    
    itemTemplate: function(value) {
        return $.makeArray(value).join(", ");
    },
    
    _createSelect: function(selected) {
        var textField = this.textField;
        var $result = $("<select>").prop("multiple", true);
        
        $.each(this.items, function(_, item) {
            var value = item[textField];
            var $opt = $("<option>").text(value);
            
            if($.inArray(value, selected) > -1) {
                $opt.attr("selected", "selected");
            }
            
            $result.append($opt);
        });
        
        return $result;
    },
    
    insertTemplate: function() {
        var insertControl = this._insertControl = this._createSelect();
        
        setTimeout(function() {
            insertControl.select2({
                minWidth: 140,
                allowClear: true
            }); 
        });
        
        return insertControl;
    },
    
    editTemplate: function(value) {
        var editControl = this._editControl = this._createSelect(value);
        
        setTimeout(function() {
            editControl.select2({
                minWidth: 140,
                allowClear: true
            });
        });
        
        return editControl;
    },
    
    insertValue: function() {
        return this._insertControl.find("option:selected").map(function() {    
            return this.selected ? $(this).text() : null;
        });
    },
    
    editValue: function() {
        return this._editControl.find("option:selected").map(function() {    
            return this.selected ? $(this).text() : null;
        });
    },
    
});

jsGrid.fields.multiselect = MultiselectField;

  $("#jsGrid-table").jsGrid({
        width: "100%",
        height: "auto",

        autoload:   true,
        editing: true,
        sorting: true,
        paging: true,
        pageSize:   5,
        pageButtonCount: 5,
        pageIndex:  1,
        controller: {
            loadData: function(filter) {
            return $.ajax({
                url: "{{ route('get')}}",
                dataType: "json"
            });
            },
            updateItem: function(item){
                return $.ajax({
                    type: "PUT",
                    url: "{{ route('update')}}",
                    data: { '_token' : '{{ csrf_token() }}',
                            'id'     : item.id,
                            'name'   : item.name,
                            'email'  : item.email,
                            'tgl_lahir' : item.tgl_lahir,
                            'jeniskelamin' : item.jeniskelamin,
                            'alamat' : item.alamat,
                            'hobi'   : item.hobi
                          }
                });
            },
            deleteItem: function(item){
                return $.ajax({
                    type: "DELETE",
                    url: "{{ route('delete')}}",
                    data: { '_token' : '{{ csrf_token() }}',
                            'id'     : item.id,
                          }
                });
            },
        },
        fields: [
            {name: "name",title: "Name", width: 50, type: "text", width: 150, validate: "required"},
            {name: "email",title: "Email"},
            {name: "tgl_lahir",title: "Tanggal Lahir", width: 50, type: "date", width: 100, validate: "required"},
            {name: "jeniskelamin", title: "Jenis Kelamin", type: "select", items: jenis, valueField: "Name", textField: "Name" },
            {name: "alamat", title: "Alamat", width: 50, type: "textarea", width: 150, validate: "required"},
            {name: "hobi", title: "Hobi", type: "select", items: Hobi, valueField: "Name", textField: "Name" },
            {{--  {name: "hobi", title: "Hobi", width: 50, type: "select", width: 150, items: Hobi, valueField: "Name", textField: "Name"},  --}}
            { type: "control" }
        ],
    });

    

  $('.load').on('click',function(){
             
             var form = $("#myForm");

              if (form[0].checkValidity() === false) {
              event.preventDefault()
              event.stopPropagation()
              }

              form.addClass('was-validated');

              form.ajaxForm({
                  beforeSend: function() { 
              },complete: function(responseText, statusText, xhr, $form)
              {
              $('#preloader').hide();
              var response = jQuery.parseJSON(responseText.responseText);
              
                  if(response.status === 'fail'){
                      sweetAlert("Oops...", response.message, "error");
                  }else{
                      swal({ title: "Progress Succes !!", text: "Hey, i will close in 2 seconds !!", timer: 2e3, showConfirmButton: !1 }); 
                      $("#jsGrid-table").jsGrid("loadData");
                      form[0].reset();
                      $('.select2able').select2('destroy');
                      $('.select2able').select2();
                  }
              }
              });

              
        }); 

  });
  
  </script>

</html>